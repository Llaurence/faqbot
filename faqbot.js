const Discord = require('discord.js')

const GUILD_ID = '349436576037732353'
const SUPPORT_CHAN_ID = '349436576037732355'
const FAQ_CHAN_ID = '403520500443119619'
const CACHE_EXPIRE_TIME_MS = 6 * 3600 * 1000
const pingMessage = `please check <#${FAQ_CHAN_ID}> before posting here!`

class Cache {
  // expire: time until an entry expires, in milliseconds
  constructor (expire) {
    this.dat = {}
    this.expire = expire
  }

  add (id) {
    this.dat[id] = Date.now()
  }

  has (id) {
    let ts = this.dat[id]
    return ts != null && Date.now() < ts + this.expire
  }
}

let userCache = new Cache(CACHE_EXPIRE_TIME_MS)
let bot = new Discord.Client()
let token = process.env.TOKEN

bot.on('ready', () => {
  console.log(`Logged in as ${bot.user.tag}`)
})

bot.on('typingStart', (channel, user) => {
  let chanId = channel.id
  let userId = user.id
  let member = channel.guild.members.get(userId)

  if (chanId != SUPPORT_CHAN_ID || member == null || member.roles.size > 1) {
    return
  }

  if (!userCache.has(userId)) {
    channel.send(pingMessage, { reply: userId })
      .catch(console.error)
  }

  userCache.add(userId)
})

process.on('SIGINT', () => {
  console.log('Logging out...')
  bot.destroy()
    .then(() => process.exit())
    .catch((err) => process.exit())
})

console.log('Logging in...')
bot.login(token)
  .catch(console.error)
